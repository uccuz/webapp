const express = require('express')
const path = require('path');
const mongoose = require('mongoose');
const methodOverride = require('method-override');
const Message = require('./models/message');
const app = express();
const port = 8080
require('dotenv').config()

const DATABASEURL= `mongodb://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@${process.env.MONGO_HOST}:27017/message?authSource=admin`;

mongoose.connect(DATABASEURL,{
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => console.log('CONNECTION SUCCESS!!'))
    .catch(err => console.log(err));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }));
app.use(methodOverride('_method'));


app.get('/', async (req, res) => {
    const messages = await Message.find({});
    res.render("index",{ messages });
})

app.post('/message', async (req, res) => {
    const message =  new Message(req.body.message);
    await message.save();
    res.redirect('/');
});

app.delete('/message/:id' , async (req,res) =>{
    const { id } = req.params;
    await Message.findByIdAndDelete(id);
    res.redirect('/');
});

app.listen(port, () => {
  console.log('Example app listening at port %s', port)
})

